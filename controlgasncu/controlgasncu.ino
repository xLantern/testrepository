//Librerias
#include <ESP8266WiFi.h>         //Conexión Wifi
#include <WiFiUdp.h>
#include <Adafruit_ADS1015.h>
#include <Firebase.h>
#include <FirebaseArduino.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <NTPClient.h>           //Cliente NTP
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <Wire.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */

//Directivas
#define FIREBASE_HOST "controlgas-nodemcu.firebaseio.com"
#define FIREBASE_AUTH "DAsvjym1UiPAuFYNfdN9WM29CHA3Gf91pE5h9OUv"

#define WIFI_SSID "LAB_Electronica_Intel"
#define WIFI_PASSWORD "itch_intel"

//Definiciones
#define ON_STATE          1
#define SYNC_STATE        1
#define TEMP_ADC          1
#define OFF_STATE         0
#define UNSYNC_STATE      0
#define PRESS_ADC         0
#define MAX_INTENTS       8
#define oneSeg            1000
#define halfSeg           500
#define MAX_RANGE         65536


//Umbrales
#define tempThreshold  53// Umbral de Temperatura ON/OFF
#define pressureThreshold 15   //Umbral de Pression (Por Definir)

//Variables
int firstTime = 0;    //Bandera para el piloto
int counter = 0;      //Intentos para conectar la red
int banRed = 0;       //Bandera de Conexion de red

//int testPress = D5;   //Pruebas Press
//int testTemp = D8;    //Pruebas Temp

int valvePin =  D7;   //Pin para activar la Valvula  13 (D7 NODEMCU)
int burnerPin = D6;   //Pin del Piloto
//int temperature = A1; //Pin Medidor de Temperatura
//int pressure = A0;    //Pin Medidor de Presion (PRUEBAS)

WiFiUDP ntpUDP; //iniciamos el cliente udp para su uso con el server NTP

// cuando creamos el cliente NTP podemos especificar el servidor al
// que nos vamos a conectar en este caso al tiempo de Windows y en el offset ponemos
// nuestra zona horaria en segundos -21600 (GMT-6)
NTPClient timeClient(ntpUDP, "time.windows.com", -21600, 6000);

void setup() {

  Wire.begin(D2, D1); //I2C SCL SDA
  ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV

  /*Entradas*/
  //  pinMode(temperature, INPUT);
  //  pinMode(pressure, INPUT);

  /*Salidas*/
  pinMode(valvePin, OUTPUT);
  pinMode(burnerPin, OUTPUT);
  //  pinMode(testTemp, OUTPUT);
  //  pinMode(testPress, OUTPUT);
  Serial.begin(9600);
  WiFiManager wifiManager;

  digitalWrite(valvePin, LOW);
  digitalWrite(burnerPin, LOW);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);  // Connect to wifi.
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
    counter++;
    if (counter > 10) {
      wifiManager.autoConnect("Ctrl + Gas");
      counter = 0 ;
      break;
    } //if
  } //while
  //wifiManager.resetSettings();  //reset saved settings

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  notificacionRed();

  Firebase.setInt("GENERAL/STATE", OFF_STATE);
  timeClient.begin();
  ads.begin();

  String SSDI  =  WiFi.SSID();
  Firebase.setString("GENERAL/SSID", SSDI);
}

void loop() {
  /*Lectura del STATUS DEL SISTEMA*/
  int systemStatus = Firebase.getInt("GENERAL/STATE");    //Status del sistema

  /*  Actualizar hora desde el servidor NTP y mandarlo a la base de datos de Firebase  */
  timeClient.update();                                              //sincronizamos con el server NTP
  Serial.println(timeClient.getFormattedTime());                    //Mostrar Hora completa en el monitor serie
  Firebase.setInt("GENERAL/timeHour", timeClient.getHours());       //Horas
  Firebase.setInt("GENERAL/timeMinute", timeClient.getMinutes());   //Minutos
  Firebase.setInt("GENERAL/timeSeconds", timeClient.getSeconds());  //Segundos

  /* LECTURA DE TEMPERATURA Y PRESION */
  double readTemp = ads.readADC_SingleEnded(TEMP_ADC);                //Lectura de Temperatura ADC1
  double readPress = ads.readADC_SingleEnded(PRESS_ADC);              //Lectura de presion ADC0

  /*                  Region de Pruebas     TEMPERATURE           */
  //  if (readTemp > tempThreshold)                     //LED si sobrepasa la temperatura
  //    digitalWrite(testTemp, HIGH);                   //only for test
  //  else
  //    digitalWrite(testTemp, LOW);

  /*  Si cuando sea hora de la alarma, apaga o prende el sistema */
  int hoursAlarma = Firebase.getInt("Alarm/Hora");
  int minutesAlarma = Firebase.getInt("Alarm/Minute");
  int hours_ON = Firebase.getInt("Encendido/Hora");
  int minutes_ON = Firebase.getInt("Encendido/Minute");

  if (timeClient.getHours() == hoursAlarma
      && timeClient.getMinutes() == minutesAlarma
      && systemStatus != OFF_STATE)
    Firebase.setInt("GENERAL/STATE", OFF_STATE);
  if (timeClient.getHours() == hours_ON
      && timeClient.getMinutes() == minutes_ON
      && systemStatus != ON_STATE)
    Firebase.setInt("GENERAL/STATE", ON_STATE);

  /* Calculo de la temperatura y actualizacion de Firebase */
  double tempReal = (5 * readTemp * 100) / 65536; //Indica la temperatura (aprox) real
  Firebase.setFloat("GENERAL/Temp", tempReal);

  /* Calculo del nivel de gas y actualizacion de Firebase */
  float voltage = (float) readPress * 3.3 / 32768.0;     // voltage at the pin of the Arduino
  float pressure_kPa = (voltage - 0.3) / 3 * 1200.0;          // voltage to pressure
  float pressure_psi = pressure_kPa * 0.14503773773020923;    // kPa to psi
  int pressReal = map(pressure_psi, 16, 174 , 0, 100);
  //  Firebase.setFloat("GENERAL/levelTank", pressReal);

  /*                  Region de Pruebas    PRESION            */
  //  if (pressReal < pressureThreshold)                 //LED si esta por debajo del umbral
  //    digitalWrite(testPress, HIGH);                   //only for test
  //  else
  //    digitalWrite(testPress, LOW);

  if (systemStatus != OFF_STATE) {
    /* Encendido del sistema*/
    digitalWrite(valvePin, HIGH);
    Firebase.setInt("GENERAL/valveStatus", ON_STATE);

    if (tempReal < tempThreshold) {
      digitalWrite(burnerPin, HIGH);
      delay(500);
      digitalWrite(burnerPin, LOW);
      firstTime++;
    }
    else
      firstTime = OFF_STATE;
    if (firstTime >= MAX_INTENTS) {
      digitalWrite(valvePin, LOW);
      firstTime = OFF_STATE;
      Firebase.setInt("GENERAL/STATE", OFF_STATE);
    }
  } else {
    digitalWrite(valvePin, LOW);
    firstTime = OFF_STATE;
    Firebase.setInt("GENERAL/valveStatus", OFF_STATE);
  }
}
void notificacionRed() {
  /*  Notificar cuando se conecte de red */
  Firebase.setInt("GENERAL/syncState", SYNC_STATE);
  delay(oneSeg);
  Firebase.setInt("GENERAL/syncState", UNSYNC_STATE);
  Serial.print("Connected");
}
